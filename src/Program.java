import java.time.LocalDateTime;
import java.util.Locale;

class Program
{
   public static void main( String[] args ) throws Exception
   {
      Locale.setDefault( new Locale( "EN_US" ) );

      var patients = new Patients();
      int count    = 0;

      Patient p = new Patient( "Jan", ++count );
      p.addWeight( LocalDateTime.of( 2000, 11, 2, 8, 15 ), 80.0, "geen bijz." );
      p.addWeight( LocalDateTime.of( 2000, 11, 2, 8, 15 ), 90.1 );
      p.addWeight( LocalDateTime.of( 2000, 11, 3, 8, 15 ), 100.0, "geen bijz." );
      patients.addPatient( p );

      p = new Patient( "Piet", ++count );
      p.addWeight( LocalDateTime.of( 2020, 1, 20, 8, 15 ), 20.0, "uitgevoerd door stageaire" );
      p.addWeight( LocalDateTime.of( 2020, 3, 21, 8, 15 ), 30.1 );
      p.addWeight( LocalDateTime.of( 2020, 6, 22, 8, 15 ), 10.0 );
      patients.addPatient( p );

      p = new Patient( "Klaas", ++count );
      p.addWeight( LocalDateTime.of( 2021, 1, 1, 8, 15 ), 120.0, "Een is te weinig" );
      patients.addPatient( p );

      p = new Patient( "Wim", ++count );
      p.addWeight( LocalDateTime.of( 2000, 4, 2, 18, 25 ), 20.0, "Kleine wim?" );
      p.addWeight( LocalDateTime.of( 2000, 4, 5, 18, 45 ), 30.1, "" );
      p.addWeight( LocalDateTime.of( 2000, 4, 8, 14, 25 ), 10.0 );
      p.addWeight( LocalDateTime.of( 2000, 4, 11, 8, 55 ), 14.0 );
      patients.addPatient( p );

      p = new Patient( "Zus", ++count );
      patients.addPatient( p );

      p = new Patient( "Jet", ++count );
      p.addWeight( LocalDateTime.of( 1999, 4, 2, 18, 25 ), 20.0 );
      p.addWeight( LocalDateTime.of( 1999, 4, 5, 18, 45 ), 30.1 );
      p.addWeight( LocalDateTime.of( 2000, 4, 11, 8, 55 ), 14.0 );
      p.addWeight( LocalDateTime.of( 2000, 4, 2, 18, 25 ), 20.0, "te laat" );
      p.addWeight( LocalDateTime.of( 2000, 4, 5, 18, 45 ), 30.1, "" );
      p.addWeight( LocalDateTime.of( 2000, 4, 8, 14, 25 ), 10.0 );
      p.addWeight( LocalDateTime.of( 2015, 4, 8, 18, 25 ), 10.0 );
      p.addWeight( LocalDateTime.of( 2023, 1, 11, 18, 55 ), 14.0 );
      patients.addPatient( p );
      patients.write();

      // Now write the patients data to a json file, and immediately load it back
      // to check the contents. Note that in 'real' code we should take care of
      // of variaous exception, such as not-existing files, no read/write access etc.

      final String filename = "myprofiles.json";
      patients.save( filename );
      patients.load( filename );
      patients.write();
   }
}
